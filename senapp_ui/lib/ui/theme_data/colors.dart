import 'package:flutter/material.dart';

//esto esta en el orden del prototipo por si se confunden con los nombres, de izquierda a derecha
final Map<String, Color> senappColors = {
  'blanco': const Color.fromRGBO(193, 196, 199, 1.0),
  'grisRancio': const Color.fromRGBO(184, 184, 184, 1.0),
  'azulClaro': const Color.fromRGBO(98, 126, 161, 1.0),
  'azul': const Color.fromRGBO(12, 67, 138, 1.0),
  'azulBotones': const Color.fromRGBO(12, 63, 129, 1.0),
  'azulfondo': const Color.fromRGBO(10, 42, 85, 1.0),
  'azulPrincipal': const Color.fromRGBO(9, 18, 32, 1.0),
  'gris': const Color.fromRGBO(58, 64, 73, 1.0),
  'grisBordes': const Color.fromRGBO(114, 113, 113, 1.0),
};
