import 'package:flutter/material.dart';

import 'package:senapp_ui/ui/theme_data/colors.dart';

final ThemeData themeData = ThemeData(
  textTheme: const TextTheme(headline6: TextStyle(fontWeight: FontWeight.bold)),
  fontFamily: 'Raleway',
  appBarTheme: AppBarTheme(
    backgroundColor: senappColors['azulPrincipal'],
    elevation: 0.0
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      primary: senappColors['azulBotones'],
    ),
  ),
);
