import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:senapp_ui/ui/theme_data/colors.dart';

class CustomTopBackground extends StatelessWidget {
  final String svgBackgroundPath;
  final bool isInfity;

  const CustomTopBackground({
    Key? key,
    required this.svgBackgroundPath,
    required this.isInfity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Container(
      height: (isInfity) ? double.infinity : screenSize.height * 0.47,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            senappColors['azulPrincipal']!,
            Theme.of(context).canvasColor,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: const [0.1, 0.10],
        ),
      ),
      child: SvgPicture.asset(
        svgBackgroundPath,
        fit: BoxFit.cover,
      ),
    );
  }
}
