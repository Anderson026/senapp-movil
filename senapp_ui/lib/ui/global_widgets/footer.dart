import 'package:flutter/material.dart';
import 'package:senapp_ui/generated/l10n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:senapp_ui/ui/theme_data/colors.dart';

class Footer extends StatelessWidget {
  const Footer({Key? key}) : super(key: key);

  final _textColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: senappColors['azulPrincipal'],
      padding: const EdgeInsets.all(15.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Text(
                    S.of(context).contactUsText,
                    style: TextStyle(
                      color: _textColor,
                      fontWeight: FontWeight.bold,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    'sennapp@gmail.com',
                    style: TextStyle(color: _textColor),
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    '300-245-69-81',
                    style: TextStyle(color: _textColor),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    S.of(context).privacyPoliciesText,
                    style: TextStyle(
                      color: _textColor,
                      decoration: TextDecoration.underline,
                    //  overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    S.of(context).termsAndConditionsText,
                    style: TextStyle(
                      color: _textColor,
                      decoration: TextDecoration.underline,
                  //    overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Container(
            width: double.infinity,
            height: 1.0,
            color: senappColors['grisBordes'],
            margin: const EdgeInsets.symmetric(vertical: 15.0),
          ),
          Column(
            children: [
              Text(
                S.of(context).socialMediaText,
                style: TextStyle(
                  color: _textColor,
                  fontWeight: FontWeight.bold,
                ),
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: 6.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    FontAwesomeIcons.facebook,
                    color: _textColor,
                    size: 30.0,
                  ),
                  const SizedBox(width: 10.0),
                  Icon(
                    FontAwesomeIcons.instagram,
                    color: _textColor,
                    size: 30.0,
                  ),
                  const SizedBox(width: 10.0),
                  Icon(
                    FontAwesomeIcons.whatsapp,
                    color: _textColor,
                    size: 30.0,
                  ),
                ],
              ),
              const SizedBox(height: 10.0),
            ],
          ),
        ],
      ),
    );
  }
}
