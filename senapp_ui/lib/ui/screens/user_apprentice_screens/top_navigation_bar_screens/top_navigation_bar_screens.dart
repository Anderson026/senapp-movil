import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/options_screens/options_screen.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/qr_scanner_screens/qr_scanner_screen.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/select_data_view_screens/select_data_view_screens.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/user_information_screens/user_information_screens.dart';
import 'components/custom_app_bar.dart';

class TopNavigationBarScreens extends StatefulWidget {
  const TopNavigationBarScreens({Key? key}) : super(key: key);
  @override
  _TopNavigationBarScreens createState() => _TopNavigationBarScreens();
}

class _TopNavigationBarScreens extends State {
  @override
  Widget build(BuildContext context) {
    return const DefaultTabController(
      length: 4,
      child: Scaffold(
          appBar: CustomAppBar(),
          body: TabBarView(children: [
            UserInformationScreens(),

            QRScannerScreen(),
                
            SelectDataViewScreens(),

            OptionsScreen(),
          ])),
    );
  }
}
