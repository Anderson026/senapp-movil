import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senapp_ui/ui/theme_data/colors.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(115.0);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: senappColors['azulPrincipal'],
      title:  Stack(
        children: const [
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: 20.0),
              child: Text(
                "APRENDIZ",
                textScaleFactor: 1.2,
                style: TextStyle(fontFamily: 'Evogria'),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 50.0),
            child: Divider(color: Colors.grey),
          ),
        ],
      ),
      centerTitle: true,
      bottom: TabBar(
        automaticIndicatorColorAdjustment: false,
        indicatorColor: senappColors['azul'],
        labelColor: senappColors['azul'],
        unselectedLabelColor: senappColors['blanco'],
        tabs: const [
          Tab(icon: Icon(Icons.home, size: 40,)),
          Tab(icon: Icon(Icons.qr_code_2, size: 40,)),
          Tab(icon: Icon(Icons.account_circle,size: 40,)),
          Tab(icon: Icon(Icons.menu,size: 40,)),
        ],
      ),
      leading: Padding(
          padding: const EdgeInsets.all(12.0),
          child: SvgPicture.asset(
            "assets/images/icons/sena.svg",
            height: 30,
            color: senappColors['gris'],
          ),
        ),
        actions: [
          Container(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.all(7.0),
            child: Text(
              "SENAPP",
              textScaleFactor: 1.3,
              style: TextStyle(
                fontFamily: 'Evogria',
                color: senappColors['gris'],
              ),
            ),
          ),
        ),
        ],
    );
  }
}
