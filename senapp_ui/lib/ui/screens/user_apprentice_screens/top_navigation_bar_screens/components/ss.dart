import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:senapp_ui/ui/theme_data/colors.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(115.0);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: senappColors['azulPrincipal'],
          title: const Text(
            "APRENDIZ",
            textScaleFactor: 1.2,
            style: TextStyle(fontFamily: 'Evogria'),
          ),
          centerTitle: true,

          bottom: TabBar(
            automaticIndicatorColorAdjustment: false,
            indicatorColor: senappColors['azul'],
            labelColor: senappColors['azul'],
            unselectedLabelColor: senappColors['blanco'],
            tabs: const [
              Tab(icon: Icon(Icons.home, size: 40,)),
              Tab(icon: Icon(Icons.qr_code_2, size: 40,)),
              Tab(icon: Icon(Icons.account_circle, size: 40,)),
              Tab(icon: Icon(Icons.menu, size: 40,)),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 55.0, left: 40.0, right: 40.0),
          child: Container(
            height: 1.0,
            color: const Color.fromRGBO(112, 112, 112, 1.0),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SvgPicture.asset(
            "assets/images/icons/sena.svg",
            height: 30,
            color: senappColors['gris'],
          ),
        ),
        Container(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.all(7.0),
            child: Text(
              "SENAPP",
              textScaleFactor: 1,
              style: TextStyle(
                fontFamily: 'Evogria',
                color: senappColors['gris'],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
