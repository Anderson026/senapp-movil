import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:senapp_ui/ui/theme_data/colors.dart';
import 'components/information_container.dart';
import 'components/profile_picture.dart';

class UserInformationScreens extends StatelessWidget {
  const UserInformationScreens({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 25.0, right: 25.0),
          child: Center(
            child: Column(
              children: [
                const ProfilePicture(image: 'assets/images/icons/Avatar1.jpg'),
                const InformationContainer(
                  text1: "Nombres:",
                  text2: "Anderson Camilo",
                ),
                const InformationContainer(
                  text1: "Apellidos:",
                  text2: "Hidalgo Ceron",
                ),
                const InformationContainer(
                  text1: "Documento Identidad:",
                  text2: "1002967632",
                ),
                const InformationContainer(
                  text1: "Rol:",
                  text2: "Aprendiz",
                ),
                const InformationContainer(
                  text1: "Correo:",
                  text2: "Andersonhi@gmail.com",
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: SvgPicture.asset("assets/images/icons/QR.svg"),
                    ),
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      color: senappColors['azulPrincipal'],
                      borderRadius: const BorderRadius.all(
                        Radius.circular(20.0),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
