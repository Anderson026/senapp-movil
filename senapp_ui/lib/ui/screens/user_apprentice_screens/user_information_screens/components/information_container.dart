import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class InformationContainer extends StatelessWidget {
  final String text1;
  final String text2;
  const InformationContainer({Key? key,required this.text1,required this.text2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            text1,
            textScaleFactor: 1.3,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          Container(
            alignment: Alignment.topLeft,
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(
                  Radius.circular(5.0),
                ),
                border: Border.all(
                  color: const Color.fromRGBO(114, 113, 113, 1.0),
                )),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(text2,textScaleFactor: 1.1,),
            ),
          ),
        ],
      ),
    );
  }
}
