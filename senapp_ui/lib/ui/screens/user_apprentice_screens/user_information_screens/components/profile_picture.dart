import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senapp_ui/ui/theme_data/colors.dart';

class ProfilePicture extends StatelessWidget {
  final String image;
  const ProfilePicture({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomRight,
      children: [
        CircleAvatar(
          backgroundColor: senappColors['azul'],
          radius: 100.0,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 95.0,
            backgroundImage: AssetImage(image),
          ),
        ),
        FloatingActionButton(
          child: const Icon(Icons.add_a_photo,size: 35,),
          elevation: 10.0,
          backgroundColor: senappColors['azul'],
          onPressed: () {},
        ),
      ],
    );
  }
}
