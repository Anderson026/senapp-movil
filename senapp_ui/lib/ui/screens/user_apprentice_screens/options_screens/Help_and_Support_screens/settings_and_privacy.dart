import 'package:flutter/material.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/options_screens/widgets/options_button.dart';


class SettingsAndPrivacy extends StatelessWidget {
  const SettingsAndPrivacy({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ayuda y Soporte Tecnico"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: const [
            OptionsButton(
              icon: Icon(
                Icons.language,
                color: Colors.white,
              ),
              text: "Seleccionar idioma",
              route: "",
            ),
            OptionsButton(
              icon: Icon(
                Icons.brightness_6,
                color: Colors.white,
              ),
              text: "Seleccionar tema",
              route: "",
            ),
            OptionsButton(
              icon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              text: "Cuneta Publica",
              route: "",
            ),

          ],
        ),
      ),
    );
  }
}
