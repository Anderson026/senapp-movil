import 'package:flutter/material.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/options_screens/widgets/options_button.dart';


class TechnicalDisplay extends StatelessWidget {
  const TechnicalDisplay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ayuda y Soporte Tecnico"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: const [
            OptionsButton(
              icon: Icon(
                Icons.info_rounded,
                color: Colors.white,
              ),
              text: "Información del App",
              route: "",
            ),
            OptionsButton(
              icon: Icon(
                Icons.report,
                color: Colors.white,
              ),
              text: "Servicio de ayuda",
              route: "",
            ),
            OptionsButton(
              icon: Icon(
                Icons.email,
                color: Colors.white,
              ),
              text: "Buzón de ayuda",
              route: "",
            ),
            OptionsButton(
              icon: Icon(
                Icons.support_agent,
                color: Colors.white,
              ),
              text: "Reportar un problema",
              route: "",
            ),
          ],
        ),
      ),
    );
  }
}


