import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senapp_ui/ui/theme_data/colors.dart';

class OptionsButton extends StatelessWidget {
  final String text;
  final Widget icon;
  final String route;

  const OptionsButton({Key? key, required this.text, required this.icon, required this.route})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, left: 70, right: 70, top: 40),
      child: SizedBox(
        width: double.infinity,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.symmetric(vertical: 15.0),
            primary: senappColors['grisRancio'],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
          onPressed: () =>
                        Navigator.pushNamed(context, route),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CircleAvatar(
                backgroundColor: senappColors['grisBordes'],
                radius: 20.0,
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 15.0,
                  child: icon,
                ),
              ),
              Text(text),
              Container(),
              Container(),
            ],
          ),
        ),
      ),
    );
  }
}
