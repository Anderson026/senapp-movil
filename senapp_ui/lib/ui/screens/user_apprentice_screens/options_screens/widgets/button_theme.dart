import 'package:flutter/material.dart';

class ButtonTheme extends StatefulWidget {
  const ButtonTheme({Key? key}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<ButtonTheme> {
  bool _lights = false;

  @override
  Widget build(BuildContext context) {
    return SwitchListTile(
      title: const Text('Lights'),
      value: _lights,
      onChanged: (bool value) {
        setState(() {
          _lights = value;
        });
      },
      secondary: const Icon(Icons.lightbulb_outline),
    );
  }
}
