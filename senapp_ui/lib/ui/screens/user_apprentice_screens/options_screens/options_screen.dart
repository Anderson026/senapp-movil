import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:senapp_ui/ui/routes/routes.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/options_screens/widgets/options_button.dart';

class OptionsScreen extends StatelessWidget {
  const OptionsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: const [
            OptionsButton(
              icon: Icon(
                Icons.help_outline,
                color: Colors.white,
              ),
              text: "Ayuda y soporte tecnico",
              route: Routes.technicalDisplay,
            ),
            OptionsButton(
              icon: Icon(
                Icons.settings,
                color: Colors.white,
              ),
              text: "Configuracion y Privacidad",
              route: Routes.settingsAndPrivacy,
            ),
          ],
        ),
      ),
      bottomNavigationBar: Stack(
        alignment: Alignment.center,
        children: [
          const Padding(
            padding: EdgeInsets.only(bottom: 150,right: 20,left: 20),
            child: Divider(
              color: Colors.grey,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () =>
                        Navigator.pushNamed(context, Routes.homeScreen),
              child: const Padding(
                padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 15.0),
                child: Text('Cerrar sesion'),
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 100),
            child: Text(
              "Privacidad términos y condiciones",
              style: TextStyle(decoration: TextDecoration.underline),
            ),
          )
        ],
      ),
    );
  }
}
