import 'package:flutter/material.dart';
import 'package:senapp_ui/ui/theme_data/colors.dart';

class ScanHistoryScreen extends StatelessWidget {
  final List<String> items = List.generate(20, (index) => 'item $index');

   ScanHistoryScreen({Key? key}) : super(key: key); 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Historial"),
        centerTitle: true,
      ),
      body: Stack(
        alignment: Alignment.bottomRight,
        children: [
          ListView.builder(
            itemCount: 20,
            itemBuilder: (context, index) {
              final item = items[index];
              return Dismissible(
                onDismissed: (_) {
                  // El parámetro no se usa temporalmente, se indica con un guión bajo
                  items.removeAt(index);
                }, // monitor
                movementDuration: const Duration(milliseconds: 100),
                key: Key(item),
                child: const ListTile(
                  title: Text('item'),
                ),
                background: Container(
                  color: Colors.red[700],
                ),
              );
            },
          ),
           Padding(
             padding: const EdgeInsets.all(20.0),
             child: FloatingActionButton(
              onPressed: () {},
              backgroundColor: senappColors['azulPrincipal'],
              child: const Icon(Icons.delete),
          ),
           ),
        ],
      ),
    );
  }
}
