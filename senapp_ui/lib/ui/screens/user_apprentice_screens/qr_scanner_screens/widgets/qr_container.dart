import 'package:flutter/material.dart';

import 'package:flutter_svg/svg.dart';

import 'package:senapp_ui/ui/theme_data/colors.dart';

class QrContainer extends StatelessWidget {
  final String qr;
  final String image;

  const QrContainer({
    Key? key,
    required this.qr,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 5.0),
      width: 400,
      height: 400,
      child: Stack(
        children: [
          Center(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(50.0),
                child: SvgPicture.asset("assets/images/icons/QR.svg"),
              ),
              width: 300,
              height: 300,
              decoration: BoxDecoration(
                color: senappColors['azulPrincipal'],
                borderRadius: const BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 300),
            child: Center(
              child: CircleAvatar(
                backgroundColor: senappColors['azul'],
                radius: 50.0,
                child: const CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 45.0,
                  backgroundImage:
                      AssetImage("assets/images/icons/Avatar1.jpg"),
                ),
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 250),
            child: Center(
              child: Text(
                "SENAPP",
                textScaleFactor: 2.0,
                style: TextStyle(fontFamily: 'Evogria', color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
