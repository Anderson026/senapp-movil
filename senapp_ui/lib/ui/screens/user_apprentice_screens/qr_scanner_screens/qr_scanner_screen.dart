import 'package:flutter/material.dart';

import 'package:senapp_ui/ui/routes/routes.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/qr_scanner_screens/widgets/qr_container.dart';
import 'package:senapp_ui/ui/theme_data/colors.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class QRScannerScreen extends StatelessWidget {
  const QRScannerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const Center(child: QrContainer(qr: "", image: "")),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () =>
                Navigator.pushNamed(context, Routes.scanHistoryScreen),
            backgroundColor: senappColors['azulPrincipal'],
            child: const Icon(Icons.history),
          ),
          const SizedBox(width: 20.0),
          FloatingActionButton(
            onPressed: ()  => scanQR(),
            backgroundColor: senappColors['azulPrincipal'],
            child: const Icon(Icons.qr_code_scanner_rounded),
          ),
        ],
      ),
    );
  }
  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#0C3F81', 'Cancel', true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }}
}
