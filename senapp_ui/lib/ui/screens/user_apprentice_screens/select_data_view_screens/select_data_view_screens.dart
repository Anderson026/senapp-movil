import 'package:flutter/material.dart';

import 'package:senapp_ui/ui/routes/routes.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/select_data_view_screens/widgets/change_view_container_data.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/select_data_view_screens/widgets/row_responsive.dart';

class SelectDataViewScreens extends StatelessWidget {
  const SelectDataViewScreens({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: const [
            Center(
                child: OrientationSwitcher(
              children: [
                ChangeViewContainerData(
                  datos: "Datos Personales",
                  route: Routes.seePersonalDataScreens),
                ChangeViewContainerData(
                  datos: "Datos Medicos",
                  route: Routes.seeMedicalDataScreens),
              ],
            )),
            OrientationSwitcher(
              children: [
                ChangeViewContainerData(
                  datos: "Datos Registro",
                  route: Routes.viewLogData),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
