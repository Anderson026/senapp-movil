import 'package:flutter/material.dart';


class ViewLogData extends StatefulWidget {
  const ViewLogData({Key? key}) : super(key: key);

  @override
  State<ViewLogData> createState() => _ViewLogDataState();
}

class _ViewLogDataState extends State<ViewLogData> {
  late TextEditingController _nameController;
  late TextEditingController _lastNameController;
  late TextEditingController _identificationDocumentController;
  late TextEditingController _fileController;
  late TextEditingController _emailController;

  final List<String> _file = ['Rol','Aprendiz', 'Instructor',];
  String _rol = 'Rol';

  final List<String> _id = ['Cedula','Documento de Identidad','Paporte'];
  String _ids = 'Cedula';


  @override
  void initState() {
    _nameController = TextEditingController();
    _lastNameController = TextEditingController();
    _identificationDocumentController = TextEditingController();
    _fileController = TextEditingController();
    _emailController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _lastNameController.dispose();
    _identificationDocumentController.dispose();
    _fileController.dispose();
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const borders = OutlineInputBorder(
      borderSide: BorderSide(
        width: 1,
        color: Colors.red,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text("Datos Registro"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 25.0, right: 25.0),
          child: Form(
            child: Column(
              children: [
                const SizedBox(height: 10),
                const _CustomDivider(innerText: 'Datos usuario'),
                const SizedBox(height: 10),
                 TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(
                    hintText: 'Nombres',
                    label: Text('Nombres:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: _lastNameController,
                  decoration: const InputDecoration(
                    hintText: 'Apellidos',
                    label: Text('Apellidos:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                DropdownButton(
                  isExpanded: true,
                  hint: const Text('Cedula:'),
                  onChanged: (newValue) {
                    _ids = newValue as String;
                    setState(() {});
                  },
                  value: _ids,
                  items: _id
                      .map((ids) => DropdownMenuItem(
                            child: Text(ids),
                            value: ids,
                          ))
                      .toList(),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  keyboardType: TextInputType.number,
                  controller: _identificationDocumentController,
                  decoration: const InputDecoration(
                    hintText: 'Documento',
                    label: Text('Documento:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: _fileController,
                  decoration: const InputDecoration(
                    hintText: 'Ficha',
                    label: Text('Ficha:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                DropdownButton(
                  isExpanded: true,
                  hint: const Text('Rol:'),
                  onChanged: (newValue) {
                    _rol = newValue as String;
                    setState(() {});
                  },
                  value: _rol,
                  items: _file
                      .map((rol) => DropdownMenuItem(
                            child: Text(rol),
                            value: rol,
                          ))
                      .toList(),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                    hintText: 'Correo electrónico',
                    label: Text('Correo electrónico:'),
                    border: borders,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: ElevatedButton(
        onPressed: () {},
        child: const Text('Actualizar Datos'),
      ),
    );
  }
}

class _CustomDivider extends StatelessWidget {
  final String innerText;

  const _CustomDivider({
    Key? key,
    this.innerText = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        const Divider(),
        Text(innerText,
            style: const TextStyle(fontSize: 12, color: Colors.black54)),
      ],
    );
  }
}
