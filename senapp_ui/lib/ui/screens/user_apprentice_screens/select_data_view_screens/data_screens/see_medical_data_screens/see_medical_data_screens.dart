import 'package:flutter/material.dart';


class SeeMedicalDataScreens extends StatefulWidget {
  const SeeMedicalDataScreens({Key? key}) : super(key: key);

  @override
  State<SeeMedicalDataScreens> createState() => _SeePersonalDataScreensState();
}

class _SeePersonalDataScreensState extends State<SeeMedicalDataScreens> {
  late TextEditingController _birthDateController;
  late TextEditingController _placeOfBirthController;
  late TextEditingController _addressController;
  late TextEditingController _phoneNumberController;
  late TextEditingController _typeOfDisabilityController;
  late TextEditingController _descriptionOfDisabilityController;
  late TextEditingController _diseaseTypeController;
  late TextEditingController _descriptionOfDiseaseController;
  late TextEditingController _measureController;

  final List<String> _sexes = ['A+', 'A-', 'O+','O-','AB-','AB+'];
  String _sex = 'A+';

  final List<String> _phoneNumberTypes = ['fijo', 'mobile',];
  String _phoneNumberType = 'fijo';

  @override
  void initState() {
    _birthDateController = TextEditingController();
    _placeOfBirthController = TextEditingController();
    _addressController = TextEditingController();
    _phoneNumberController = TextEditingController();
    _typeOfDisabilityController = TextEditingController();
    _descriptionOfDisabilityController = TextEditingController();
    _diseaseTypeController = TextEditingController();
    _descriptionOfDiseaseController = TextEditingController();
    _measureController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _birthDateController.dispose();
    _placeOfBirthController.dispose();
    _addressController.dispose();
    _phoneNumberController.dispose();
    _descriptionOfDisabilityController.dispose();
    _diseaseTypeController.dispose();
    _descriptionOfDiseaseController.dispose();
    _measureController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const borders = OutlineInputBorder(
      borderSide: BorderSide(
        width: 1,
        color: Colors.red,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text("Datos Medicos"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 25.0, right: 25.0),
          child: Form(
            child: Column(
              children: [
                const SizedBox(height: 20),
                DropdownButton(
                  isExpanded: true,
                  hint: const Text('RH:'),
                  onChanged: (newValue) {
                    _sex = newValue as String;
                    setState(() {});
                  },
                  value: _sex,
                  items: _sexes
                      .map((sex) => DropdownMenuItem(
                            child: Text(sex),
                            value: sex,
                          ))
                      .toList(),
                ),
                const SizedBox(height: 10),
                const _CustomDivider(innerText: 'Contacto EPS'),
                const SizedBox(height: 10),
                TextFormField(
                  controller: _placeOfBirthController,
                  decoration: const InputDecoration(
                    hintText: 'EPS',
                    label: Text('Nombre EPS:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: _addressController,
                  decoration: const InputDecoration(
                    hintText: 'Direccion',
                    label: Text('Direccion:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                DropdownButton(
                  isExpanded: true,
                  hint: const Text('Tipo numero de telefono:'),
                  onChanged: (newValue) {
                    _phoneNumberType = newValue as String;
                    setState(() {});
                  },
                  value: _phoneNumberType,
                  items: _phoneNumberTypes
                      .map((type) => DropdownMenuItem(
                            child: Text(type),
                            value: type,
                          ))
                      .toList(),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  keyboardType: TextInputType.number,
                  controller: _phoneNumberController,
                  decoration: const InputDecoration(
                    hintText: 'Numero de telefono',
                    label: Text('Numero de telefono:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 10),
                const _CustomDivider(innerText: 'Discapacidades'),
                const SizedBox(height: 10),
                TextFormField(
                  controller: _typeOfDisabilityController,
                  decoration: const InputDecoration(
                    hintText: 'tipo de discapacidad',
                    label: Text('tipo de discapacidad:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: _descriptionOfDisabilityController,
                  decoration: const InputDecoration(
                    hintText: 'Descripcion de discapacidad',
                    label: Text('Descripcion de discapacidad:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 10),
                const _CustomDivider(innerText: 'Enfermedades'),
                const SizedBox(height: 10),
                TextFormField(
                  controller: _diseaseTypeController,
                  decoration: const InputDecoration(
                    hintText: 'tipo de Enfermedad',
                    label: Text('tipo de Enfermedad:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: _descriptionOfDiseaseController,
                  decoration: const InputDecoration(
                    hintText: 'Descripcion de Enfermedad',
                    label: Text('Descripcion de Enfermedad:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: _measureController,
                  decoration: const InputDecoration(
                    hintText: 'Mediciona',
                    label: Text('Mediciona:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 40),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: ElevatedButton(
        onPressed: () {},
        child: const Text('Actualizar Datos'),
      ),
    );
  }
}

class _CustomDivider extends StatelessWidget {
  final String innerText;

  const _CustomDivider({
    Key? key,
    this.innerText = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        const Divider(),
        Text(innerText,
            style: const TextStyle(fontSize: 12, color: Colors.black54)),
      ],
    );
  }
}
