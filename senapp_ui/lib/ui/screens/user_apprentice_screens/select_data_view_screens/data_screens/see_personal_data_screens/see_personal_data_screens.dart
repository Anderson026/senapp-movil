import 'package:flutter/material.dart';

import 'package:intl/intl.dart';

class SeePersonalDataScreens extends StatefulWidget {
  const SeePersonalDataScreens({Key? key}) : super(key: key);

  @override
  State<SeePersonalDataScreens> createState() => _SeePersonalDataScreensState();
}

class _SeePersonalDataScreensState extends State<SeePersonalDataScreens> {
  late TextEditingController _birthDateController;
  late TextEditingController _placeOfBirthController;
  late TextEditingController _addressController;
  late TextEditingController _phoneNumberController;

  final List<String> _sexes = ['Masculino', 'Femenino', 'otro'];
  String _sex = 'Masculino';

  final List<String> _phoneNumberTypes = ['Fijo', 'Movile'];
  String _phoneNumberType = 'Fijo';

  @override
  void initState() {
    _birthDateController = TextEditingController();
    _placeOfBirthController = TextEditingController();
    _addressController = TextEditingController();
    _phoneNumberController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _birthDateController.dispose();
    _placeOfBirthController.dispose();
    _addressController.dispose();
    _phoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const borders = OutlineInputBorder(
      borderSide: BorderSide(
        width: 1,
        color: Colors.red,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: const Text("Datos Personales"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 25.0, right: 25.0),
          child: Form(
            child: Column(
              children: [
                const SizedBox(height: 20),
                DropdownButton(
                  isExpanded: true,
                  hint: const Text('Sexo:'),
                  onChanged: (newValue) {
                    _sex = newValue as String;
                    setState(() {});
                  },
                  value: _sex,
                  items: _sexes
                      .map((sex) => DropdownMenuItem(
                            child: Text(sex),
                            value: sex,
                          ))
                      .toList(),
                ),
                const SizedBox(height: 10),
                const _CustomDivider(innerText: 'Namiciento'),
                const SizedBox(height: 10),
                TextFormField(
                  controller: _birthDateController,
                  decoration: InputDecoration(
                    hintText: 'Fecha de nacimiento',
                    label: const Text('Fecha de Nacimiento:'),
                    border: borders,
                    suffixIcon: IconButton(
                      onPressed: () async {
                        final dateFormat = DateFormat('yyyy-MM-dd');

                        final birthDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2000, 1, 1),
                          lastDate: DateTime.now(),
                        );

                        if (birthDate != null) {
                          _birthDateController.text =
                              dateFormat.format(birthDate);
                        }
                        
                        setState(() {});
                      },
                      icon: const Icon(Icons.calendar_today),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: _placeOfBirthController,
                  decoration: const InputDecoration(
                    hintText: 'Lugar de Nacimiento',
                    label: Text('Lugar de Nacimiento:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 10),
                const _CustomDivider(innerText: 'Contacto'),
                const SizedBox(height: 10),
                TextFormField(
                  controller: _addressController,
                  decoration: const InputDecoration(
                    hintText: 'Direccion',
                    label: Text('Direccion:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
                DropdownButton(
                  isExpanded: true,
                  hint: const Text('Tipo numero de telefono:'),
                  onChanged: (newValue) {
                    _phoneNumberType = newValue as String;
                    setState(() {});
                  },
                  value: _phoneNumberType,
                  items: _phoneNumberTypes
                      .map((type) => DropdownMenuItem(
                            child: Text(type),
                            value: type,
                          ))
                      .toList(),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  keyboardType: TextInputType.number,
                  controller: _phoneNumberController,
                  decoration: const InputDecoration(
                    hintText: 'Numero de telefono',
                    label: Text('Numero de telefono:'),
                    border: borders,
                  ),
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: ElevatedButton(
        onPressed: () {},
        child: const Text('Actualizar Datos'),
      ),
    );
  }
}

class _CustomDivider extends StatelessWidget {
  final String innerText;

  const _CustomDivider({
    Key? key,
    this.innerText = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        const Divider(),
        Text(innerText,
            style: const TextStyle(fontSize: 12, color: Colors.black54)),
      ],
    );
  }
}
