import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrientationSwitcher extends StatelessWidget {
  final List<Widget> children;

  const OrientationSwitcher({Key? key, required this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isScreenWide =
        MediaQuery.of(context).size.width >= 380;

    return Flex(
      mainAxisAlignment: MainAxisAlignment.center,
      direction: isScreenWide ? Axis.horizontal : Axis.vertical,
      children: children,
    );
  }
}
