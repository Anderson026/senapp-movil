import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:senapp_ui/ui/theme_data/colors.dart';

class ChangeViewContainerData extends StatelessWidget {
  final String route;
  final String datos;

  const ChangeViewContainerData({
    Key? key,
    required this.route,
    required this.datos,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 60, left: 7, right: 7),
      child: Container(
        height: 220,
        width: 170,
        decoration: BoxDecoration(
          color: senappColors['azulPrincipal'],
          borderRadius: const BorderRadius.all(
            Radius.circular(15.0),
          ),
        ),
        child: Stack(
          children: [
            SvgPicture.asset(
              'assets/images/background/backgroundContainerData.svg',
              color: senappColors['azulfondo'],
              fit: BoxFit.cover,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 180),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  FloatingActionButton(
                    child: const Icon(
                      Icons.edit,
                      size: 35,
                    ),
                    elevation: 2.0,
                    backgroundColor: Colors.transparent,
                    onPressed: () =>
                        Navigator.pushNamed(context, route),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Center(
                child: Column(
                  children: [
                     Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: Text(
                        datos,
                        textScaleFactor: 1,
                        style: const TextStyle(
                          fontFamily: 'Evogria',
                          color: Colors.white,
                        ),
                    ),
                     ),
                    const Divider(color: Colors.grey,),
                    const Text(
                      "Sexo: Masculino",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    const Divider(color: Colors.grey,),
                    const Text(
                      "Edad: 21",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    const Divider(color: Colors.grey,),
                    const Text(
                      "Fecha de nacimiento: 08/09/21",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    const Divider(color: Colors.grey,),
                    const Text(
                      "ficha: 2141297",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
