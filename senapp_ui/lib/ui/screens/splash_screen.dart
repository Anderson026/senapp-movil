import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:senapp_ui/ui/screens/home_screen/home_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: SvgPicture.asset(
        'assets/images/icons/senappSimpleLogo.svg',
        height: 60.0,
        width: 100.0,
      ),
      nextScreen: const HomeScreen(),
    );
  }
}
