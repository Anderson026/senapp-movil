import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:senapp_ui/ui/global_widgets/custom_top_background.dart';
import 'package:senapp_ui/generated/l10n.dart';

class LoginHeader extends StatelessWidget {
  const LoginHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const CustomTopBackground(
          svgBackgroundPath: 'assets/images/background/backgroundLogin.svg',
          isInfity: false,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 35.0),
                  child: Text(
                    S.of(context).loginTopMessage,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 40.0,
                    ),
                  ),
                ),
              ),
              SvgPicture.asset(
                'assets/images/common/qrImage.svg',
                placeholderBuilder: (_) => Image.asset(
                  'assets/loadings/loading1.gif',
                  height: 150.0,
                  width: 130.0,
                ),
                width: 120,
                height: 130,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
