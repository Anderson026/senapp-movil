import 'package:flutter/material.dart';
import 'package:senapp_ui/generated/l10n.dart';

import 'package:senapp_ui/ui/theme_data/colors.dart';
import 'package:senapp_ui/ui/routes/routes.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool passwordVisibility = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: Form(
        child: Column(
          children: [
            TextFormField(
              controller: _emailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                hintText: S.of(context).emailHintText,
                prefixIcon: const Icon(Icons.email_outlined),
                suffixIcon: const Icon(Icons.done),
              ),
            ),
            const SizedBox(height: 10.0),
            TextFormField(
              controller: _passwordController,
              keyboardType: TextInputType.visiblePassword,
              obscureText: passwordVisibility ? false : true,
              decoration: InputDecoration(
                hintText: S.of(context).passwordHintText,
                prefixIcon: const Icon(Icons.lock_outline),
                suffixIcon: IconButton(
                  onPressed: () {
                    passwordVisibility
                        ? setState(() => passwordVisibility = false)
                        : setState(() => passwordVisibility = true);
                  },
                  icon: (passwordVisibility)
                      ? const Icon(Icons.visibility_outlined)
                      : const Icon(Icons.visibility_off_outlined),
                ),
              ),
            ),
            const _LoginButtons(),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }
}

class _LoginButtons extends StatelessWidget {
  const _LoginButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const SizedBox(height: 15.0),
        Text(
          S.of(context).forgotPassword,
          style: TextStyle(
            color: senappColors['azulBotones'],
            fontWeight: FontWeight.w700,
          ),
        ),
        const SizedBox(height: 25.0),
        Center(
          child: Column(
            children: [
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                  ),
                  onPressed: () =>
                      Navigator.pushNamed(context, Routes.topNavigationBarScreens),
                  child: Text(S.of(context).loginButton),
                ),
              ),
              const SizedBox(height: 15.0),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                  ),
                  onPressed: () =>
                      Navigator.pushNamed(context, Routes.registerScreen),
                  child: Text(S.of(context).registerButton),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
