import 'package:flutter/material.dart';

import 'package:senapp_ui/ui/screens/login_screen/widgets/login_form.dart';
import 'package:senapp_ui/ui/screens/login_screen/widgets/login_header.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Column(
          children: const [
            LoginHeader(),
            LoginForm(),
          ],
        ),
      ),
    );
  }
}
