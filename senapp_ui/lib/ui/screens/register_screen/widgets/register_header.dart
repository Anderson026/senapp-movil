import 'package:flutter/material.dart';
import 'package:senapp_ui/generated/l10n.dart';

class RegisterHeader extends StatelessWidget {
  const RegisterHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
      child: Column(
        children: [
          Text(
            S.of(context).registerRecommendationTitle,
            style: Theme.of(context).textTheme.headline6,
          ),
          const SizedBox(height: 15.0),
          Text(
            S.of(context).recommendationOne,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          Text(
            S.of(context).recommendationTwo,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          Text(
            S.of(context).recommendationThree,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ],
      ),
    );
  }
}
