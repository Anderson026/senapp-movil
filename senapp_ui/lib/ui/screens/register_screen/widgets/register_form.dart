import 'package:flutter/material.dart';
import 'package:senapp_ui/generated/l10n.dart';

import 'package:senapp_ui/ui/theme_data/colors.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _idController = TextEditingController();
  final _nameContorller = TextEditingController();
  final _surnameContorller = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  bool passwordVisibility = false;
  bool confirmPasswordVisibility = false;
  bool acceptTermnsAndConditions = false;

  final _idTypes = [
    'Documento Identidad',
    'Cedula Ciudadanía',
    'Tarjeta de identidad',
    'Cedula Extranjería',
  ];
  String _idType = 'Documento Identidad';

  final _userRols = [
    'Rol',
    'Aprendiz',
    'Instructor',
  ];
  String _userRol = 'Rol';

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0),
      child: Form(
        child: Column(
          children: [
            _idTypeDropdown(),
            const SizedBox(height: 10.0),
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (valor) {
                if (valor == 2.toString()) {
                  return 'todo mal papi';
                }
              },
              controller: _idController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: S.of(context).identificationDocumentHintText,
                prefixIcon: const Icon(Icons.assignment_ind_outlined),
              ),
            ),
            const SizedBox(height: 10.0),
            TextFormField(
              controller: _nameContorller,
              keyboardType: TextInputType.name,
              decoration: InputDecoration(
                hintText: S.of(context).nameHintText,
                prefixIcon: const Icon(Icons.person_outlined),
              ),
            ),
            const SizedBox(height: 10.0),
            TextFormField(
              controller: _surnameContorller,
              keyboardType: TextInputType.name,
              decoration: InputDecoration(
                hintText: S.of(context).lastNameHintText,
                prefixIcon: const Icon(Icons.person_outlined),
              ),
            ),
            const SizedBox(height: 10.0),
            _roleDropdown(),
            const SizedBox(height: 10.0),
            TextFormField(
              controller: _emailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                hintText: S.of(context).emailHintText,
                prefixIcon: const Icon(Icons.email_outlined),
              ),
            ),
            const SizedBox(height: 10.0),
            TextFormField(
              controller: _passwordController,
              keyboardType: TextInputType.visiblePassword,
              obscureText: passwordVisibility ? false : true,
              decoration: InputDecoration(
                hintText: S.of(context).passwordHintText,
                prefixIcon: const Icon(Icons.lock_outline),
                suffixIcon: IconButton(
                  onPressed: () {
                    passwordVisibility
                        ? setState(() => passwordVisibility = false)
                        : setState(() => passwordVisibility = true);
                  },
                  icon: (passwordVisibility)
                      ? const Icon(Icons.visibility_outlined)
                      : const Icon(Icons.visibility_off_outlined),
                ),
              ),
            ),
            const SizedBox(height: 10.0),
            TextFormField(
              controller: _confirmPasswordController,
              keyboardType: TextInputType.visiblePassword,
              obscureText: confirmPasswordVisibility ? false : true,
              decoration: InputDecoration(
                hintText: S.of(context).confirmPasswordHintText,
                prefixIcon: const Icon(Icons.lock_outline),
                suffixIcon: IconButton(
                  onPressed: () {
                    confirmPasswordVisibility
                        ? setState(() => confirmPasswordVisibility = false)
                        : setState(() => confirmPasswordVisibility = true);
                  },
                  icon: (confirmPasswordVisibility)
                      ? const Icon(Icons.visibility_outlined)
                      : const Icon(Icons.visibility_off_outlined),
                ),
              ),
            ),
            const SizedBox(height: 10.0),
            _termsAndConditionsCheckBox(),
            _registerButton(context),
          ],
        ),
      ),
    );
  }

  Widget _idTypeDropdown() {
    return SizedBox(
      width: double.infinity,
      child: DropdownButton<String>(
        iconSize: 30.0,
        icon: Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Icon(
                Icons.expand_more,
                color: senappColors['grisBorders'],
              ),
            ],
          ),
        ),
        items: _convertToDropDownMenuItems(_idTypes),
        onChanged: (newValue) {
          setState(() => _idType = newValue!);
        },
        value: _idType,
        underline: Container(
          color: senappColors['grisRancio'],
          height: 1.0,
        ),
      ),
    );
  }

  Widget _roleDropdown() {
    return SizedBox(
      width: double.infinity,
      child: DropdownButton<String>(
        iconSize: 30.0,
        icon: Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Icon(
                Icons.expand_more,
                color: senappColors['grisBorders'],
              ),
            ],
          ),
        ),
        items: _convertToDropDownMenuItems(_userRols),
        onChanged: (newValue) {
          setState(() => _userRol = newValue!);
        },
        value: _userRol,
        underline: Container(
          color: senappColors['grisRancio'],
          height: 1.0,
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> _convertToDropDownMenuItems(
      List<String> listToConvert) {
    final idTypesItems = listToConvert.map((idType) {
      return DropdownMenuItem<String>(
        child: Text(idType),
        value: idType,
      );
    }).toList();
    return idTypesItems;
  }

  Widget _registerButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Center(
        child: SizedBox(
          width: double.infinity,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
            ),
            onPressed: () {},
            child: Text(S.of(context).registerButton),
          ),
        ),
      ),
    );
  }

  Widget _termsAndConditionsCheckBox() {
    final checkbox = Checkbox(
        value: acceptTermnsAndConditions,
        onChanged: (newValue) {
          setState(() {
            acceptTermnsAndConditions
                ? acceptTermnsAndConditions = false
                : acceptTermnsAndConditions = true;
          });
        });
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        checkbox,
        const SizedBox(width: 20.0),
        Row(
          children: [
            Text(S.of(context).acceptText),
            Text(
              S.of(context).termsAndConditionsText,
              style: const TextStyle(decoration: TextDecoration.underline),
              overflow: TextOverflow.ellipsis,
            ),
          ],
        )
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();
    _idController.dispose();
    _nameContorller.dispose();
    _surnameContorller.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
  }
}
