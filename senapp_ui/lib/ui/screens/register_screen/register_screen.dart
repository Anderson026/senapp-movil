import 'package:flutter/material.dart';

import 'package:senapp_ui/ui/theme_data/colors.dart';
import 'package:senapp_ui/ui/global_widgets/custom_top_background.dart';
import 'package:senapp_ui/ui/global_widgets/footer.dart';
import 'package:senapp_ui/generated/l10n.dart';
import 'package:senapp_ui/ui/screens/register_screen/widgets/register_form.dart';
import 'package:senapp_ui/ui/screens/register_screen/widgets/register_header.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          const _CustomSliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              const [
                RegisterHeader(),
                RegisterForm(),
                SizedBox(height: 20.0),
                Footer(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _CustomSliverAppBar extends StatelessWidget {
  const _CustomSliverAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return SliverAppBar(
      elevation: 0.0,
      backgroundColor: senappColors['azulPrincipal'],
      expandedHeight: screenSize.height * 0.3,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        background: Stack(
          children: [
            const CustomTopBackground(
              svgBackgroundPath:
                  'assets/images/background/backgroundRegister.svg',
              isInfity: true,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30.0,
                vertical: 70.0,
              ),
              child: Text(
                S.of(context).registerTopMessage,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 38.0,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
