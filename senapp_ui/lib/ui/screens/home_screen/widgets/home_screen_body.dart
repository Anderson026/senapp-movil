import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:senapp_ui/generated/l10n.dart';

class HomeScreenBody extends StatelessWidget {
  const HomeScreenBody({Key? key}) : super(key: key);

  final textStyle = const TextStyle(fontSize: 16.0);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Text(S.of(context).homeTitle,
                  style: Theme.of(context).textTheme.headline6),
              const SizedBox(height: 10.0),
              Text(
                S.of(context).senappDescription,
                style: textStyle,
                textAlign: TextAlign.justify,
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SvgPicture.asset('assets/images/background/starLeft.svg'),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Text(S.of(context).missionTitle,
                        style: Theme.of(context).textTheme.headline6),
                    const SizedBox(height: 10.0),
                    Text(
                      S.of(context).missionDescription,
                      style: textStyle,
                      textAlign: TextAlign.justify,
                    ),
                    const SizedBox(height: 15.0),
                    Text(S.of(context).visionTitle,
                        style: Theme.of(context).textTheme.headline6),
                    const SizedBox(height: 10.0),
                    Text(
                      S.of(context).visionDescription,
                      style: textStyle,
                      textAlign: TextAlign.justify,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
