import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:senapp_ui/generated/l10n.dart';

import 'package:senapp_ui/ui/theme_data/colors.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Drawer(
        child: Column(
          children: [
            const _DrawerHeader(),
            _DrawerOptions(),
            Expanded(child: Container(color: Colors.transparent)),
            _DrawerFooter(),
          ],
        ),
      ),
    );
  }
}

class _DrawerHeader extends StatelessWidget {
  const _DrawerHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: senappColors['gris'],
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 30.0),
        child: SvgPicture.asset(
          'assets/images/icons/senappLogo.svg',
          width: 170.0,
          height: 130.0,
        ),
      ),
    );
  }
}

class _DrawerOptions extends StatelessWidget {
  _DrawerOptions({Key? key}) : super(key: key);

  final separationLine = Container(
    color: senappColors['grisBordes'],
    height: 1.0,
  );

  final butonTextstyle = TextStyle(
    color: senappColors['azulPrincipal'],
    fontWeight: FontWeight.bold,
  );

  final buttonStyle = const ButtonStyle(
    alignment: Alignment.centerLeft,
  );

  final buttonTextsPadin = const EdgeInsets.symmetric(vertical: 12.0);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: double.infinity,
          child: TextButton(
            style: buttonStyle,
            onPressed: () {},
            child: Padding(
              padding: buttonTextsPadin,
              child: Text(
                S.of(context).homeButtonText,
                style: butonTextstyle,
                textAlign: TextAlign.start,
              ),
            ),
          ),
        ),
        separationLine,
        SizedBox(
          width: double.infinity,
          child: TextButton(
            style: buttonStyle,
            onPressed: () {},
            child: Padding(
              padding: buttonTextsPadin,
              child: Text(
                S.of(context).aboutUsButtonText,
                style: butonTextstyle,
              ),
            ),
          ),
        ),
        separationLine,
        SizedBox(
          width: double.infinity,
          child: TextButton(
            style: buttonStyle,
            onPressed: () {},
            child: Padding(
              padding: buttonTextsPadin,
              child: Text(
                S.of(context).miniTutorialsButtonText,
                style: butonTextstyle,
              ),
            ),
          ),
        ),
        separationLine,
      ],
    );
  }
}

class _DrawerFooter extends StatelessWidget {
  _DrawerFooter({Key? key}) : super(key: key);

  final separationLine = Container(
    color: senappColors['grisBordes'],
    height: 1.0,
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: separationLine,
        ),
        Padding(
          padding: const EdgeInsets.all(25.0),
          child: Text(
            S.of(context).termsAndConditionsText,
            style: const TextStyle(
              decoration: TextDecoration.underline,
            ),
          ),
        ),
      ],
    );
  }
}
