import 'package:flutter/material.dart';

import 'package:senapp_ui/ui/routes/routes.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size(double.infinity, 60.0);

  const CustomAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      actions: [
        IconButton(
          onPressed: () {
            Navigator.pushNamed(context, Routes.loginScreen);
          },
          icon: const Icon(
            Icons.login,
          ),
        )
      ],
      title: const Text(
        'SENAPP',
        style: TextStyle(
          fontFamily: 'evogria',
          fontSize: 25.0,
        ),
      ),
      centerTitle: true,
    );
  }
}
