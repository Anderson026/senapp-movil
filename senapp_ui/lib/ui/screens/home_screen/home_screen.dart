import 'package:flutter/material.dart';
import 'package:senapp_ui/ui/global_widgets/footer.dart';

import 'package:senapp_ui/ui/screens/home_screen/widgets/custom_app_bar.dart';
import 'package:senapp_ui/ui/screens/home_screen/widgets/custom_carousel_slider.dart';
import 'package:senapp_ui/ui/screens/home_screen/widgets/custom_drawer.dart';
import 'package:senapp_ui/ui/screens/home_screen/widgets/home_screen_body.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      appBar: const CustomAppBar(),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: const [
            CustomCarouselSlider(),
            HomeScreenBody(),
            SizedBox(height: 10.0),
            Footer(),
          ],
        ),
      ),
    );
  }
}
