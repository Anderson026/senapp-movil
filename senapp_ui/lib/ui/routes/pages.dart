import 'package:flutter/material.dart';

import 'package:senapp_ui/ui/screens/home_screen/home_screen.dart';
import 'package:senapp_ui/ui/screens/login_screen/login_screen.dart';
import 'package:senapp_ui/ui/screens/register_screen/register_screen.dart';
import 'package:senapp_ui/ui/screens/splash_screen.dart';
import 'package:senapp_ui/ui/routes/routes.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/options_screens/Help_and_Support_screens/settings_and_privacy.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/options_screens/Help_and_Support_screens/technical_display.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/qr_scanner_screens/widgets/scan_history_screen.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/select_data_view_screens/data_screens/see_medical_data_screens/see_medical_data_screens.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/select_data_view_screens/data_screens/see_personal_data_screens/see_personal_data_screens.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/select_data_view_screens/data_screens/view_log_data/view_log_data.dart';
import 'package:senapp_ui/ui/screens/user_apprentice_screens/top_navigation_bar_screens/top_navigation_bar_screens.dart';

abstract class Pages {
  static const String initialRoute = Routes.splashScreen;

  static final Map<String, Widget Function(BuildContext)> routes = {
    Routes.splashScreen           : (_) => const SplashScreen(),
    Routes.homeScreen             : (_) => const HomeScreen(),
    Routes.loginScreen            : (_) => const LoginScreen(),
    Routes.registerScreen         : (_) => const RegisterScreen(),
    Routes.topNavigationBarScreens: (_) => const TopNavigationBarScreens(),
    Routes.seePersonalDataScreens : (_) =>  const SeePersonalDataScreens(),
    Routes.scanHistoryScreen      : (_) =>  ScanHistoryScreen(),
    Routes.seeMedicalDataScreens   : (_) =>  const SeeMedicalDataScreens(),
    Routes.viewLogData   : (_) =>  const ViewLogData(),
    Routes.technicalDisplay   : (_) =>  const TechnicalDisplay(),
    Routes.settingsAndPrivacy   : (_) =>  const SettingsAndPrivacy(),
  };
}
