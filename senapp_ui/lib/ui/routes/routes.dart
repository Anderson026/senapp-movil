abstract class Routes {
  static const splashScreen   = 'splash-screen';
  static const homeScreen     = '/';
  static const loginScreen    = 'login';
  static const registerScreen = 'register';
  static const topNavigationBarScreens = 'top-navigation';
  static const seePersonalDataScreens = 'see-Personal-Data-Screens';
  static const scanHistoryScreen = 'Scan-History-Screen';
  static const seeMedicalDataScreens = 'See-Medical-Data-Screens';
  static const viewLogData = 'View-Log-Data';
  static const technicalDisplay = 'technical-Display';
  static const settingsAndPrivacy = 'settings-AndPrivacy';
}
