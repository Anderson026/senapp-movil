import 'package:flutter/material.dart';
import 'package:senapp_ui/generated/l10n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:senapp_ui/ui/routes/pages.dart';
import 'package:senapp_ui/ui/theme_data/theme_data.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: themeData,
      title: 'Senapp',
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: S.delegate.supportedLocales,
      initialRoute: Pages.initialRoute,
      routes: Pages.routes,
    );
  }
}
