// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "aboutUsButtonText":
            MessageLookupByLibrary.simpleMessage("Sobre Nosotros"),
        "acceptText": MessageLookupByLibrary.simpleMessage("Acepto los "),
        "confirmPasswordHintText":
            MessageLookupByLibrary.simpleMessage("Confirmar Contraseña"),
        "contactUsText": MessageLookupByLibrary.simpleMessage("Contactanos"),
        "emailHintText": MessageLookupByLibrary.simpleMessage("Correo"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("¿Olvidaste tu contraseña?"),
        "homeButtonText": MessageLookupByLibrary.simpleMessage("Inicio"),
        "homeTitle": MessageLookupByLibrary.simpleMessage("¿Que es Senapp?"),
        "identificationDocumentHintText":
            MessageLookupByLibrary.simpleMessage("Documento Identidad"),
        "identificationDocumentTypeHintText":
            MessageLookupByLibrary.simpleMessage("Tipo Documento Identidad"),
        "lastNameHintText": MessageLookupByLibrary.simpleMessage("Apellido"),
        "loginButton": MessageLookupByLibrary.simpleMessage("Iniciar Sesion"),
        "loginTopMessage":
            MessageLookupByLibrary.simpleMessage("Bienvenido De Vuelta"),
        "miniTutorialsButtonText":
            MessageLookupByLibrary.simpleMessage("Mini Tutoriales"),
        "missionDescription": MessageLookupByLibrary.simpleMessage(
            "Nuestra misión es brindar a la comunidad del SENA-CTPI una app mobil que facilite el acceso a la informacion de los estudiantes e instructores, garantizando y respetando siempre la privacidad de los usuarios."),
        "missionTitle": MessageLookupByLibrary.simpleMessage("Misión"),
        "nameHintText": MessageLookupByLibrary.simpleMessage("Nombre"),
        "passwordHintText": MessageLookupByLibrary.simpleMessage("Contraseña"),
        "privacyPoliciesText":
            MessageLookupByLibrary.simpleMessage("Políticas de Privacidad"),
        "recommendationOne": MessageLookupByLibrary.simpleMessage(
            "1. Debes leer y estar de acuerdo con los terminos y condiciones, porfavor, leelos"),
        "recommendationThree": MessageLookupByLibrary.simpleMessage(
            "3. si estas de acuerdo con lo anterior mencinado, bienvenido a nuestra comunidad"),
        "recommendationTwo": MessageLookupByLibrary.simpleMessage(
            "2. Debes leer y estar de acuerdo con las politicas de privacidad antes de crear una cuenta"),
        "registerButton": MessageLookupByLibrary.simpleMessage("Registrarse"),
        "registerRecommendationTitle": MessageLookupByLibrary.simpleMessage(
            "Antes de registrarte ten en cuenta lo siguiente:"),
        "registerTopMessage":
            MessageLookupByLibrary.simpleMessage("Crea Una\nCuenta"),
        "roleHintText": MessageLookupByLibrary.simpleMessage("Rol"),
        "senappDescription": MessageLookupByLibrary.simpleMessage(
            "Sennap es una aplicacion mobil desarrollada para los estudiantes del SENA-CTPI con el fin de facilitar el acceso a la informacion de sus estudiantes e instructores, por medio de un codigo QR que al ser escaneado redirige a una base de datos con la informacion registrada por el estudiante o instructor en primera instancia, se puede usar como una simple base de datos para almacenar informacion personal si asi se deseas."),
        "socialMediaText":
            MessageLookupByLibrary.simpleMessage("Redes Sociales"),
        "termsAndConditionsText":
            MessageLookupByLibrary.simpleMessage("términos y condiciones"),
        "visionDescription": MessageLookupByLibrary.simpleMessage(
            "Nuestra visión es llegar en un plazo de 2-3 años llegar a mas instituciones educativas o instancias del SENA, para que puedan hacer uso de nuestra app mobile, SENAPP"),
        "visionTitle": MessageLookupByLibrary.simpleMessage("Visión")
      };
}
