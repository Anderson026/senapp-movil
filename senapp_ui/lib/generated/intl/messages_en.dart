// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "aboutUsButtonText": MessageLookupByLibrary.simpleMessage("About Us"),
        "acceptText": MessageLookupByLibrary.simpleMessage("I Accept the "),
        "confirmPasswordHintText":
            MessageLookupByLibrary.simpleMessage("Confirm Password"),
        "contactUsText": MessageLookupByLibrary.simpleMessage("Contact Us"),
        "emailHintText": MessageLookupByLibrary.simpleMessage("Email"),
        "forgotPassword":
            MessageLookupByLibrary.simpleMessage("Forgot your password?"),
        "homeButtonText": MessageLookupByLibrary.simpleMessage("Home"),
        "homeTitle": MessageLookupByLibrary.simpleMessage("What is Senapp?"),
        "identificationDocumentHintText":
            MessageLookupByLibrary.simpleMessage("Identification Document"),
        "identificationDocumentTypeHintText":
            MessageLookupByLibrary.simpleMessage(
                "Identification Document Type"),
        "lastNameHintText": MessageLookupByLibrary.simpleMessage("Last Name"),
        "loginButton": MessageLookupByLibrary.simpleMessage("Login"),
        "loginTopMessage": MessageLookupByLibrary.simpleMessage("Welcome Back"),
        "miniTutorialsButtonText":
            MessageLookupByLibrary.simpleMessage("Mini Tutorials"),
        "missionDescription": MessageLookupByLibrary.simpleMessage(
            "Our mission is to provide the SENA-CTPI community with a mobile app that facilitates access to information for students and instructors, always guaranteeing and respecting the privacy of users."),
        "missionTitle": MessageLookupByLibrary.simpleMessage("Mission"),
        "nameHintText": MessageLookupByLibrary.simpleMessage("Name"),
        "passwordHintText": MessageLookupByLibrary.simpleMessage("Password"),
        "privacyPoliciesText":
            MessageLookupByLibrary.simpleMessage("Privacy Policies"),
        "recommendationOne": MessageLookupByLibrary.simpleMessage(
            "1. You must read and agree to the terms and conditions, please, read them"),
        "recommendationThree": MessageLookupByLibrary.simpleMessage(
            "3. If you agree with the above, welcome to our community"),
        "recommendationTwo": MessageLookupByLibrary.simpleMessage(
            "2. You must read and agree to the privacy policies before creating an account"),
        "registerButton": MessageLookupByLibrary.simpleMessage("Register"),
        "registerRecommendationTitle": MessageLookupByLibrary.simpleMessage(
            "Before registering, keep the following in mind:"),
        "registerTopMessage":
            MessageLookupByLibrary.simpleMessage("Create An\nAccount"),
        "roleHintText": MessageLookupByLibrary.simpleMessage("Role"),
        "senappDescription": MessageLookupByLibrary.simpleMessage(
            "Sennap is a mobile application developed for SENA-CTPI students in order to facilitate access to information for their students and instructors, through a QR code that, when scanned, redirects to a database with the information registered by the student or instructor in the first instance, it can be used as a simple database to store personal information if desired."),
        "socialMediaText": MessageLookupByLibrary.simpleMessage("Social Media"),
        "termsAndConditionsText":
            MessageLookupByLibrary.simpleMessage("Terms and Conditions"),
        "visionDescription": MessageLookupByLibrary.simpleMessage(
            "Our vision is to reach more educational institutions or instances of SENA within 2-3 years, so that they can make use of our mobile app, SENAPP."),
        "visionTitle": MessageLookupByLibrary.simpleMessage("Vision")
      };
}
