// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `¿Que es Senapp?`
  String get homeTitle {
    return Intl.message(
      '¿Que es Senapp?',
      name: 'homeTitle',
      desc: '',
      args: [],
    );
  }

  /// `Sennap es una aplicacion mobil desarrollada para los estudiantes del SENA-CTPI con el fin de facilitar el acceso a la informacion de sus estudiantes e instructores, por medio de un codigo QR que al ser escaneado redirige a una base de datos con la informacion registrada por el estudiante o instructor en primera instancia, se puede usar como una simple base de datos para almacenar informacion personal si asi se deseas.`
  String get senappDescription {
    return Intl.message(
      'Sennap es una aplicacion mobil desarrollada para los estudiantes del SENA-CTPI con el fin de facilitar el acceso a la informacion de sus estudiantes e instructores, por medio de un codigo QR que al ser escaneado redirige a una base de datos con la informacion registrada por el estudiante o instructor en primera instancia, se puede usar como una simple base de datos para almacenar informacion personal si asi se deseas.',
      name: 'senappDescription',
      desc: '',
      args: [],
    );
  }

  /// `Misión`
  String get missionTitle {
    return Intl.message(
      'Misión',
      name: 'missionTitle',
      desc: '',
      args: [],
    );
  }

  /// `Nuestra misión es brindar a la comunidad del SENA-CTPI una app mobil que facilite el acceso a la informacion de los estudiantes e instructores, garantizando y respetando siempre la privacidad de los usuarios.`
  String get missionDescription {
    return Intl.message(
      'Nuestra misión es brindar a la comunidad del SENA-CTPI una app mobil que facilite el acceso a la informacion de los estudiantes e instructores, garantizando y respetando siempre la privacidad de los usuarios.',
      name: 'missionDescription',
      desc: '',
      args: [],
    );
  }

  /// `Visión`
  String get visionTitle {
    return Intl.message(
      'Visión',
      name: 'visionTitle',
      desc: '',
      args: [],
    );
  }

  /// `Nuestra visión es llegar en un plazo de 2-3 años llegar a mas instituciones educativas o instancias del SENA, para que puedan hacer uso de nuestra app mobile, SENAPP`
  String get visionDescription {
    return Intl.message(
      'Nuestra visión es llegar en un plazo de 2-3 años llegar a mas instituciones educativas o instancias del SENA, para que puedan hacer uso de nuestra app mobile, SENAPP',
      name: 'visionDescription',
      desc: '',
      args: [],
    );
  }

  /// `Bienvenido De Vuelta`
  String get loginTopMessage {
    return Intl.message(
      'Bienvenido De Vuelta',
      name: 'loginTopMessage',
      desc: '',
      args: [],
    );
  }

  /// `Correo`
  String get emailHintText {
    return Intl.message(
      'Correo',
      name: 'emailHintText',
      desc: '',
      args: [],
    );
  }

  /// `Contraseña`
  String get passwordHintText {
    return Intl.message(
      'Contraseña',
      name: 'passwordHintText',
      desc: '',
      args: [],
    );
  }

  /// `¿Olvidaste tu contraseña?`
  String get forgotPassword {
    return Intl.message(
      '¿Olvidaste tu contraseña?',
      name: 'forgotPassword',
      desc: '',
      args: [],
    );
  }

  /// `Iniciar Sesion`
  String get loginButton {
    return Intl.message(
      'Iniciar Sesion',
      name: 'loginButton',
      desc: '',
      args: [],
    );
  }

  /// `Registrarse`
  String get registerButton {
    return Intl.message(
      'Registrarse',
      name: 'registerButton',
      desc: '',
      args: [],
    );
  }

  /// `Crea Una\nCuenta`
  String get registerTopMessage {
    return Intl.message(
      'Crea Una\nCuenta',
      name: 'registerTopMessage',
      desc: '',
      args: [],
    );
  }

  /// `Antes de registrarte ten en cuenta lo siguiente:`
  String get registerRecommendationTitle {
    return Intl.message(
      'Antes de registrarte ten en cuenta lo siguiente:',
      name: 'registerRecommendationTitle',
      desc: '',
      args: [],
    );
  }

  /// `1. Debes leer y estar de acuerdo con los terminos y condiciones, porfavor, leelos`
  String get recommendationOne {
    return Intl.message(
      '1. Debes leer y estar de acuerdo con los terminos y condiciones, porfavor, leelos',
      name: 'recommendationOne',
      desc: '',
      args: [],
    );
  }

  /// `2. Debes leer y estar de acuerdo con las politicas de privacidad antes de crear una cuenta`
  String get recommendationTwo {
    return Intl.message(
      '2. Debes leer y estar de acuerdo con las politicas de privacidad antes de crear una cuenta',
      name: 'recommendationTwo',
      desc: '',
      args: [],
    );
  }

  /// `3. si estas de acuerdo con lo anterior mencinado, bienvenido a nuestra comunidad`
  String get recommendationThree {
    return Intl.message(
      '3. si estas de acuerdo con lo anterior mencinado, bienvenido a nuestra comunidad',
      name: 'recommendationThree',
      desc: '',
      args: [],
    );
  }

  /// `Tipo Documento Identidad`
  String get identificationDocumentTypeHintText {
    return Intl.message(
      'Tipo Documento Identidad',
      name: 'identificationDocumentTypeHintText',
      desc: '',
      args: [],
    );
  }

  /// `Documento Identidad`
  String get identificationDocumentHintText {
    return Intl.message(
      'Documento Identidad',
      name: 'identificationDocumentHintText',
      desc: '',
      args: [],
    );
  }

  /// `Nombre`
  String get nameHintText {
    return Intl.message(
      'Nombre',
      name: 'nameHintText',
      desc: '',
      args: [],
    );
  }

  /// `Apellido`
  String get lastNameHintText {
    return Intl.message(
      'Apellido',
      name: 'lastNameHintText',
      desc: '',
      args: [],
    );
  }

  /// `Rol`
  String get roleHintText {
    return Intl.message(
      'Rol',
      name: 'roleHintText',
      desc: '',
      args: [],
    );
  }

  /// `Confirmar Contraseña`
  String get confirmPasswordHintText {
    return Intl.message(
      'Confirmar Contraseña',
      name: 'confirmPasswordHintText',
      desc: '',
      args: [],
    );
  }

  /// `Acepto los `
  String get acceptText {
    return Intl.message(
      'Acepto los ',
      name: 'acceptText',
      desc: '',
      args: [],
    );
  }

  /// `términos y condiciones`
  String get termsAndConditionsText {
    return Intl.message(
      'términos y condiciones',
      name: 'termsAndConditionsText',
      desc: '',
      args: [],
    );
  }

  /// `Políticas de Privacidad`
  String get privacyPoliciesText {
    return Intl.message(
      'Políticas de Privacidad',
      name: 'privacyPoliciesText',
      desc: '',
      args: [],
    );
  }

  /// `Contactanos`
  String get contactUsText {
    return Intl.message(
      'Contactanos',
      name: 'contactUsText',
      desc: '',
      args: [],
    );
  }

  /// `Redes Sociales`
  String get socialMediaText {
    return Intl.message(
      'Redes Sociales',
      name: 'socialMediaText',
      desc: '',
      args: [],
    );
  }

  /// `Inicio`
  String get homeButtonText {
    return Intl.message(
      'Inicio',
      name: 'homeButtonText',
      desc: '',
      args: [],
    );
  }

  /// `Sobre Nosotros`
  String get aboutUsButtonText {
    return Intl.message(
      'Sobre Nosotros',
      name: 'aboutUsButtonText',
      desc: '',
      args: [],
    );
  }

  /// `Mini Tutoriales`
  String get miniTutorialsButtonText {
    return Intl.message(
      'Mini Tutoriales',
      name: 'miniTutorialsButtonText',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
